#ifndef ELEMENTSORT_H
#define ELEMENTSORT_H
#include <QString>

class ElementSort
{
    public:
        ElementSort();
        QString getName();
        void setName(QString aName);
    private:
        QString mName;
//    signals:

//    public slots:
};

#endif // ELEMENTSORT_H
