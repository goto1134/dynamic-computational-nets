<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en_GB">
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation>Динамические вычислительные сети</translation>
    </message>
    <message>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CreateDVS</source>
        <translation>Создать ДВС</translation>
    </message>
    <message>
        <source>Create new DVS model</source>
        <translation>Создать новую модель ДВС</translation>
    </message>
    <message>
        <source>LoadDVS</source>
        <translation>Загрузить ДВС</translation>
    </message>
    <message>
        <source>Load DVS Project</source>
        <translation>Загрузить ДВС проект</translation>
    </message>
    <message>
        <source>saveDVS</source>
        <translation>Созранить ДВС</translation>
    </message>
    <message>
        <source>Save DVS</source>
        <translation>Сохранить ДВС проект</translation>
    </message>
    <message>
        <source>Coursor</source>
        <translation>Курсор</translation>
    </message>
    <message>
        <source>Place</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <source>Transition</source>
        <translation>Переход</translation>
    </message>
    <message>
        <source>RegularConnection</source>
        <translation>Связь</translation>
    </message>
    <message>
        <source>ControlConnection</source>
        <translation>Управляющая связь</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <source>Garbage</source>
        <translation>Сборка мусора</translation>
    </message>
    <message>
        <source>Check</source>
        <translation>Проверка корректности</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <source>StepForward</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <source>StepBack</source>
        <translation>Назад</translation>
    </message>
</context>
</TS>
